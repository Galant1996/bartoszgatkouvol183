﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseDragHandler : MonoBehaviour {

	private Vector3 screenPoint; // Stores a position in world space
	private Vector3 offset; // Stores an offset

	void OnMouseDown()
	{
		// Get the start position of a game object
		screenPoint = Camera.main.WorldToScreenPoint(gameObject.transform.position);

		// Calculate the offset
		offset = gameObject.transform.position - Camera.main.ScreenToWorldPoint(new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z));
	}

    void OnMouseDrag()
    {
		// Update current position of a game object
        Vector3 currentScreenPoint = new Vector3(Input.mousePosition.x, Input.mousePosition.y, screenPoint.z);
        Vector3 currentPosition = Camera.main.ScreenToWorldPoint(currentScreenPoint) + offset;
        transform.position = currentPosition;

    }

}

