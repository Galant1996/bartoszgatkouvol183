**Technical Test**

The aim of this project is to show skills used in Unity Games Engine and C# programming.
The project should be made of two given tasks.
Solutons for particular task is presented by particular scene in the Scene folder.

---
## Task 1 - Zadanie 1

Task one required to implement a simple Newton's cradle by using Unity Physics System. A user can drag a ball by clicking a ball and moving it in any direction. When the mouse left-button is relased, the ball impacts the other balls in the row.

Scripts:
MouseDragHandler - This is responsible for draging a game object in the world space

---
## Task 2 - Zadanie 2

Unfinished